package com.rabobank.tools.customerstatementprocessor.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.rabobank.tools.customerstatementprocessor.Config;
import com.rabobank.tools.customerstatementprocessor.IFileProcessor;
import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;
import com.rabobank.tools.customerstatementprocessor.parsers.IFileParser;
import com.rabobank.tools.customerstatementprocessor.util.FileUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerStatementProcessorApplicationTests {

	@Autowired
	@Qualifier("csvFileParser")
	private IFileParser csvFileParser;

	@Autowired
	@Qualifier("xmlFileParser")
	private IFileParser xmlFileParser;
	@Autowired
	private Config config;

	@Autowired
	private IFileProcessor iFileProcessor;

	@Test
	public void configTest() {

		assertEquals(
				"Config [outputfilename=report, outputfilenameextenstion=.csv, outputfileheader=TransactionReference,TransactionDescription,File,ErrorDescription]",
				config.toString());
	}

	@Test
	public void parsingCSVTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records.csv").toURI());
		List<Record> records = csvFileParser.parseFile(path);

		assertEquals(3, records.size());
	}

	@Test
	public void parsingXMLTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records.xml").toURI());
		List<Record> records = xmlFileParser.parseFile(path);

		assertEquals(3, records.size());
	}

	@Test
	public void csvDuplicateTransactionRefTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records.csv").toURI());
		List<Record> records = csvFileParser.parseFile(path);

		records = FileUtil.getDuplicatesTransactionReferanceNumbers(records);

		assertEquals(1, records.size());

		assertEquals(115625, records.get(0).getTransactionReferanceNumber());
	}

	@Test
	public void xmlDuplicateTransactionRefTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records.xml").toURI());
		List<Record> records = xmlFileParser.parseFile(path);

		records = FileUtil.getDuplicatesTransactionReferanceNumbers(records);

		assertEquals(1, records.size());

		assertEquals(176737, records.get(0).getTransactionReferanceNumber());
	}

	@Test
	public void csvEndbalanceMismatchTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records.csv").toURI());
		List<Record> records = csvFileParser.parseFile(path);

		records = FileUtil.getRecodsWhosEndBalanceMismatch(records);

		assertEquals(1, records.size());

		assertEquals(103403, records.get(0).getTransactionReferanceNumber());
	}

	@Test
	public void xmlEndbalanceMismatchTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records.xml").toURI());
		List<Record> records = xmlFileParser.parseFile(path);

		records = FileUtil.getRecodsWhosEndBalanceMismatch(records);

		assertEquals(1, records.size());

		assertEquals(128191, records.get(0).getTransactionReferanceNumber());
	}

	@Test
	public void csvNoErrorTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records -noerror.csv").toURI());
		List<Record> records = csvFileParser.parseFile(path);

		records = FileUtil.getRecodsWhosEndBalanceMismatch(records);

		assertEquals(0, records.size());

		records = csvFileParser.parseFile(path);

		records = FileUtil.getDuplicatesTransactionReferanceNumbers(records);

		assertEquals(0, records.size());

	}

	@Test
	public void xmlNoErrorTest() throws URISyntaxException, ApplicationProcessorException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path path = Paths.get(classLoader.getResource("testdata/records -noerror.xml").toURI());
		List<Record> records = xmlFileParser.parseFile(path);
		records = FileUtil.getRecodsWhosEndBalanceMismatch(records);

		assertEquals(0, records.size());

		records = xmlFileParser.parseFile(path);

		records = FileUtil.getDuplicatesTransactionReferanceNumbers(records);

		assertEquals(0, records.size());

	}

	@Test()
	public void completeApplicationTest() throws URISyntaxException, ApplicationProcessorException, IOException {

		ClassLoader classLoader = getClass().getClassLoader();
		Path inputdirectory = Paths.get(classLoader.getResource("testdata").toURI());
		Path outputdirectory = Paths.get(classLoader.getResource("testdata").toURI());

		String reporfile = FileUtil.getOutputFileName(
				outputdirectory.toString().concat(File.separator + config.getOutputfilename()),
				config.getOutputfilenameextenstion());
		iFileProcessor.processFileAndGenerateReport(inputdirectory.toString(), outputdirectory.toString());

		// this will throw an excption because report file is used by another program
		// and we can read that shows
		// file exists
		assertTrue(new File(reporfile).canRead());

	}
}
