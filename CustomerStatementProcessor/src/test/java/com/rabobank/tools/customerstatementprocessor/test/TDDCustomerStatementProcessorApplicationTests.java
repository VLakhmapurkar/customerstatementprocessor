package com.rabobank.tools.customerstatementprocessor.test;

import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;
import com.rabobank.tools.customerstatementprocessor.parsers.IFileParser;
import com.rabobank.tools.customerstatementprocessor.util.FileUtil;

public class TDDCustomerStatementProcessorApplicationTests {

	@Mock
	private IFileParser iFileParser;
	@Mock
	private FileUtil fileUtil;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	// Test parser interface
	@Test
	public void testIFileParser() throws ApplicationProcessorException {

		List<Record> mockList = new ArrayList<>();
		Record rec = Mockito.mock(Record.class);
		rec.setTransactionReferanceNumber(12345);
		mockList.add(rec);

		when(iFileParser.parseFile(Paths.get("test.csv"))).thenReturn(mockList);
	}

}
