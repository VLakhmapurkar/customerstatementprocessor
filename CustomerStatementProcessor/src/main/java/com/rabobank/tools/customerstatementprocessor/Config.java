package com.rabobank.tools.customerstatementprocessor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Class Config.
 */
@Configuration
@EnableConfigurationProperties
@ComponentScan("com.rabobank.tools.customerstatementprocessor")
@ConfigurationProperties("curstomerstatementprocessor")
public class Config {

	/** The outputfilename. */
	private String outputfilename;
	
	/** The outputfilenameextenstion. */
	private String outputfilenameextenstion;
	
	/** The outputfileheader. */
	private String outputfileheader;

	/**
	 * Gets the outputfilename.
	 *
	 * @return the outputfilename
	 */
	public String getOutputfilename() {
		return outputfilename;
	}

	/**
	 * Sets the outputfilename.
	 *
	 * @param outputfilename the new outputfilename
	 */
	public void setOutputfilename(String outputfilename) {
		this.outputfilename = outputfilename;
	}

	/**
	 * Gets the outputfilenameextenstion.
	 *
	 * @return the outputfilenameextenstion
	 */
	public String getOutputfilenameextenstion() {
		return outputfilenameextenstion;
	}

	/**
	 * Sets the outputfilenameextenstion.
	 *
	 * @param outputfilenameextenstion the new outputfilenameextenstion
	 */
	public void setOutputfilenameextenstion(String outputfilenameextenstion) {
		this.outputfilenameextenstion = outputfilenameextenstion;
	}

	/**
	 * Gets the outputfileheader.
	 *
	 * @return the outputfileheader
	 */
	public String getOutputfileheader() {
		return outputfileheader;
	}

	/**
	 * Sets the outputfileheader.
	 *
	 * @param outputfileheader the new outputfileheader
	 */
	public void setOutputfileheader(String outputfileheader) {
		this.outputfileheader = outputfileheader;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Config [outputfilename=" + outputfilename + ", outputfilenameextenstion=" + outputfilenameextenstion
				+ ", outputfileheader=" + outputfileheader + "]";
	}

}