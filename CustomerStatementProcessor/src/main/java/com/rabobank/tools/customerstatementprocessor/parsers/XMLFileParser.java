package com.rabobank.tools.customerstatementprocessor.parsers;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;

/**
 * The Class XMLFileParser.
 */
@Component
@Qualifier("xmlFileParser")
public class XMLFileParser implements IFileParser {

	/* (non-Javadoc)
	 * @see com.rabobank.tools.customerstatementprocessor.parsers.IFileParser#parseFile(java.nio.file.Path)
	 */
	@Override
	public List<Record> parseFile(Path inputFile) throws ApplicationProcessorException {
		List<Record> rerords = new ArrayList<Record>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			// Load the input XML document, parse it and return an instance of the
			// Document class.
			Document document = builder.parse(inputFile.toFile());

			NodeList nodeList = document.getDocumentElement().getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element elem = (Element) node;

					Record rec = new Record();

					rec.setTransactionReferanceNumber(
							Integer.parseInt(node.getAttributes().getNamedItem("reference").getNodeValue()));

					rec.setTransactionDescription(
							elem.getElementsByTagName("description").item(0).getChildNodes().item(0).getNodeValue());

					rec.setAccountnumber(
							elem.getElementsByTagName("accountNumber").item(0).getChildNodes().item(0).getNodeValue());

					rec.setStartBalance(BigDecimal.valueOf(Double.parseDouble(
							elem.getElementsByTagName("startBalance").item(0).getChildNodes().item(0).getNodeValue())));
					rec.setMutation(BigDecimal.valueOf(Double.parseDouble(
							elem.getElementsByTagName("mutation").item(0).getChildNodes().item(0).getNodeValue())));
					rec.setEndBalance(BigDecimal.valueOf(Double.parseDouble(
							elem.getElementsByTagName("endBalance").item(0).getChildNodes().item(0).getNodeValue())));
					rerords.add(rec);
				}
			}

		} catch (Exception e) {
			throw new ApplicationProcessorException("XMl parsing error " + e.getLocalizedMessage(), e);
		}

		if (rerords.size() > 0)
			rerords.stream().forEach(rec -> rec.setFileName(inputFile.getFileName().toString()));

		return rerords;
	}

}
