package com.rabobank.tools.customerstatementprocessor.exception;

/**
 * The Class ApplicationProcessorException.
 */
public class ApplicationProcessorException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new application processor exception.
	 *
	 * @param msg the msg
	 */
	public ApplicationProcessorException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new application processor exception.
	 *
	 * @param msg the msg
	 * @param cause the cause
	 */
	public ApplicationProcessorException(String msg, Throwable cause) {
		super(msg, cause);
	}
}