package com.rabobank.tools.customerstatementprocessor.beans;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * The Class Record.
 */
public class Record {
	
	/** The transaction referance number. */
	int transactionReferanceNumber;
	
	/** The start balance. */
	BigDecimal startBalance;
	
	/** The mutation. */
	BigDecimal mutation;
	
	/** The end balance. */
	BigDecimal endBalance;
	
	/** The transaction description. */
	String transactionDescription;
	
	/** The file name. */
	String fileName;
	
	/** The error description. */
	String errorDescription;
	
	/** The accountnumber. */
	String accountnumber;

	/**
	 * Gets the accountnumber.
	 *
	 * @return the accountnumber
	 */
	public String getAccountnumber() {
		return accountnumber;
	}

	/**
	 * Sets the accountnumber.
	 *
	 * @param accountnumber the new accountnumber
	 */
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}

	/**
	 * Gets the transaction referance number.
	 *
	 * @return the transaction referance number
	 */
	public int getTransactionReferanceNumber() {
		return transactionReferanceNumber;
	}

	/**
	 * Sets the transaction referance number.
	 *
	 * @param transactionReferanceNumber the new transaction referance number
	 */
	public void setTransactionReferanceNumber(int transactionReferanceNumber) {
		this.transactionReferanceNumber = transactionReferanceNumber;
	}

	/**
	 * Gets the start balance.
	 *
	 * @return the start balance
	 */
	public BigDecimal getStartBalance() {
		return startBalance;
	}

	/**
	 * Sets the start balance.
	 *
	 * @param startBalance the new start balance
	 */
	public void setStartBalance(BigDecimal startBalance) {
		this.startBalance = startBalance;
	}

	/**
	 * Gets the mutation.
	 *
	 * @return the mutation
	 */
	public BigDecimal getMutation() {
		return mutation;
	}

	/**
	 * Sets the mutation.
	 *
	 * @param mutation the new mutation
	 */
	public void setMutation(BigDecimal mutation) {
		this.mutation = mutation;
	}

	/**
	 * Gets the end balance.
	 *
	 * @return the end balance
	 */
	public BigDecimal getEndBalance() {
		return endBalance;
	}

	/**
	 * Sets the end balance.
	 *
	 * @param endBalance the new end balance
	 */
	public void setEndBalance(BigDecimal endBalance) {
		this.endBalance = endBalance;
	}

	/**
	 * Gets the transaction description.
	 *
	 * @return the transaction description
	 */
	public String getTransactionDescription() {
		return transactionDescription;
	}

	/**
	 * Sets the transaction description.
	 *
	 * @param transactionDescription the new transaction description
	 */
	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the error description.
	 *
	 * @return the error description
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * Sets the error description.
	 *
	 * @param errorDescription the new error description
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return transactionReferanceNumber + "," + transactionDescription + "," + fileName + "," + errorDescription;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(transactionReferanceNumber);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		return Objects.equals(transactionReferanceNumber, other.transactionReferanceNumber);

	}

}
