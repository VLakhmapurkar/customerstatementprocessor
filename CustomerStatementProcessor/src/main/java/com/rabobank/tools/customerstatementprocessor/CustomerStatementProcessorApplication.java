package com.rabobank.tools.customerstatementprocessor;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;

/**
 * The Class CustomerStatementProcessorApplication.
 */
@SpringBootApplication
public class CustomerStatementProcessorApplication implements CommandLineRunner {

	/** The config. */
	@Autowired
	private Config config;

	/** The log. */
	private static Logger log = Logger.getLogger(CustomerStatementProcessorApplication.class);

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException {

		log.info("Sarting CustomerStatementProcessorApplication");
		log.info("Checking prerequisites..");
		if (args.length < 2) {
			log.warn("Please  provide input file directory and output file directory as command line arguments..");
			log.info("you can modify output file name in application.properties file, default is : report.csv");
			log.fatal(
					"Usage: java com.rabobank.tools.customerStatementProcessor.CustomerStatementProcessorApplication <inputfilesdirectory> <outputfiledirectory>");
			System.exit(2);

		}
		log.info("prerequisites done..");

		ApplicationContext context = SpringApplication.run(CustomerStatementProcessorApplication.class, args);

		IFileProcessor fileProcessor = context.getBean("fileProcessor", FileProcessor.class);

		try {
			fileProcessor.processFileAndGenerateReport(args[0], args[1]);
		} catch (ApplicationProcessorException e) {
			log.error("Error occured in application hence exiting," + e.getLocalizedMessage(), e);
		}

		log.info("CustomerStatementProcessorApplication Ended");

	}

	/* (non-Javadoc)
	 * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
	 */
	@Override
	public void run(String... args) throws Exception {
		log.info("Configuration loaded...");
		log.info(config.toString());

	}

}
