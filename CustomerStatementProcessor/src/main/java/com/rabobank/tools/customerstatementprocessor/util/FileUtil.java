package com.rabobank.tools.customerstatementprocessor.util;

import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.rabobank.tools.customerstatementprocessor.beans.Record;

// TODO: Auto-generated Javadoc
/**
 * The Class FileUtil.
 */
@Component
public class FileUtil {

	/**
	 * Filter end balance mismatch.
	 *
	 * @return the predicate
	 */
	public static Predicate<Record> filterEndBalanceMismatch() {
		return rec -> rec.getEndBalance().compareTo(rec.getStartBalance().add(rec.getMutation())) != 0;

	}

	/**
	 * Check CSV file.
	 *
	 * @return the predicate
	 */
	public static Predicate<Path> checkCSVFile() {
		return file -> file.toString().endsWith(".csv");
	}

	/**
	 * Filter only CSV or XML file.
	 *
	 * @return the predicate
	 */
	public static Predicate<Path> filterOnlyCSVOrXMLFile(String outputFile) {
		return file -> ((file.toString().endsWith(".csv") || file.toString().endsWith(".xml")) && !file.toString().equalsIgnoreCase(outputFile) );
	}

	/**
	 * Check XML file.
	 *
	 * @return the predicate
	 */
	public static Predicate<Path> checkXMLFile() {
		return file -> file.toString().endsWith(".xml");
	}

	/**
	 * Gets the duplicates transaction referance numbers.
	 *
	 * @param records the records
	 * @return the duplicates transaction referance numbers
	 */
	public static List<Record> getDuplicatesTransactionReferanceNumbers(List<Record> records) {

		List<Record> rawRecs = records.stream().collect(Collectors.groupingBy(Record::getTransactionReferanceNumber))
				.entrySet().stream().filter(e -> e.getValue().size() > 1).flatMap(e -> e.getValue().stream())
				.collect(Collectors.toList());
		rawRecs = rawRecs.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList());
		rawRecs.stream().forEach(rec -> rec.setErrorDescription("Transaction reference not unique"));
		return rawRecs;
	}

	/**
	 * Gets the recods whos end balance mismatch.
	 *
	 * @param records the records
	 * @return the recods whos end balance mismatch
	 */
	public static List<Record> getRecodsWhosEndBalanceMismatch(List<Record> records) {
		records = records.stream().filter(filterEndBalanceMismatch()).collect(Collectors.toList());
		records.stream().forEach(rec -> rec.setErrorDescription("Incorrect end balance"));
		return records;
	}

	/**
	 * Gets the output file name.
	 *
	 * @param outputfile the outputfile
	 * @param filext the filext
	 * @return the output file name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String getOutputFileName(String outputfile, String filext) throws IOException {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return outputfile.concat("-" + dateFormat.format(date)).concat(filext);
	}
}
