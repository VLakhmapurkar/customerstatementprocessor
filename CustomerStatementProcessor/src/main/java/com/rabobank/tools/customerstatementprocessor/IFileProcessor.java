package com.rabobank.tools.customerstatementprocessor;

import java.nio.file.Path;
import java.util.List;

import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;
import com.rabobank.tools.customerstatementprocessor.parsers.IFileParser;

/**
 * The Interface IFileProcessor.
 */
public interface IFileProcessor {

	/**
	 * Process file and generate report.
	 *
	 * @param inputDirectoryPath the input directory path
	 * @param outputFile the output file
	 * @throws ApplicationProcessorException the application processor exception
	 */
	void processFileAndGenerateReport(String inputDirectoryPath, String outputFile)
			throws ApplicationProcessorException;

	/**
	 * Gets the file parser.
	 *
	 * @param filePath the file path
	 * @return the file parser
	 */
	IFileParser getFileParser(Path filePath);

	/**
	 * Inits the report writer.
	 *
	 * @param outputfile the outputfile
	 * @throws ApplicationProcessorException the application processor exception
	 */
	void initReportWriter(String outputfile) throws ApplicationProcessorException;

	/**
	 * Generate report data.
	 *
	 * @param mainrecords the mainrecords
	 * @throws ApplicationProcessorException the application processor exception
	 */
	void generateReportData(List<Record> mainrecords) throws ApplicationProcessorException;
}
