package com.rabobank.tools.customerstatementprocessor.parsers;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;

/**
 * The Class CSVFileParser.
 */
@Component
@Qualifier("csvFileParser")
public class CSVFileParser implements IFileParser {

	/* (non-Javadoc)
	 * @see com.rabobank.tools.customerstatementprocessor.parsers.IFileParser#parseFile(java.nio.file.Path)
	 */
	@Override
	public List<Record> parseFile(Path inputFile) throws ApplicationProcessorException {
		List<Record> records = new ArrayList<Record>();
		try {
			records = Files.lines(inputFile, StandardCharsets.ISO_8859_1).skip(1).map(mapToItem)
					.collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}

		records.stream().forEach(rec -> rec.setFileName(inputFile.getFileName().toString()));

		return records;
	}

	/** The map to item. */
	private Function<String, Record> mapToItem = (line) -> {
		String[] p = line.split(",");// a CSV has comma separated lines
		Record item = new Record();
		item.setTransactionReferanceNumber(Integer.parseInt(p[0]));
		item.setAccountnumber(p[1]);
		item.setTransactionDescription(p[2]);
		item.setStartBalance(BigDecimal.valueOf(Double.parseDouble(p[3])));
		item.setMutation(BigDecimal.valueOf(Double.parseDouble(p[4])));
		item.setEndBalance(BigDecimal.valueOf(Double.parseDouble(p[5])));
		// more initialization goes here
		return item;
	};
}
