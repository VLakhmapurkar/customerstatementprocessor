package com.rabobank.tools.customerstatementprocessor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;
import com.rabobank.tools.customerstatementprocessor.parsers.IFileParser;
import com.rabobank.tools.customerstatementprocessor.util.FileUtil;

/**
 * The Class FileProcessor.
 */
@Component
public class FileProcessor implements IFileProcessor {
	
	/** The log. */
	private static Logger log = Logger.getLogger(FileProcessor.class);
	
	/** The csv file parser. */
	@Autowired
	@Qualifier("csvFileParser")
	private IFileParser csvFileParser;

	/** The xml file parser. */
	@Autowired
	@Qualifier("xmlFileParser")
	private IFileParser xmlFileParser;

	/** The config. */
	@Autowired
	private Config config;

	/** The bw. */
	private BufferedWriter bw = null;
	
	/** The fw. */
	private FileWriter fw = null;

	/* (non-Javadoc)
	 * @see com.rabobank.tools.customerstatementprocessor.IFileProcessor#processFileAndGenerateReport(java.lang.String, java.lang.String)
	 */
	@Override
	public void processFileAndGenerateReport(String inputDirectoryPath, String outputFile)
			throws ApplicationProcessorException {
		
		try {
			outputFile = FileUtil.getOutputFileName(outputFile.concat(File.separator + config.getOutputfilename()),
					config.getOutputfilenameextenstion());
		} catch (IOException e2) {
			throw new ApplicationProcessorException(e2.getLocalizedMessage(), e2);
		}
		
		
		initReportWriter(outputFile);

		Consumer<Path> filePath = (Path path) -> {
			try {
				processFile(path);
			} catch (ApplicationProcessorException e1) {
				log.error(e1.getLocalizedMessage(), e1);
			}
		};
		try {
			Files.list(Paths.get(inputDirectoryPath)).filter(FileUtil.filterOnlyCSVOrXMLFile(outputFile)).forEach(filePath);

		} catch (IOException e) {
			throw new ApplicationProcessorException(e.getLocalizedMessage(), e);
		}

		closeWriter();

	}

	/**
	 * Process file.
	 *
	 * @param filePath the file path
	 * @throws ApplicationProcessorException the application processor exception
	 */
	private void processFile(Path filePath) throws ApplicationProcessorException {

		log.info("Starting processing file " + filePath.getFileName());
		IFileParser fileParser = getFileParser(filePath);

		List<Record> mainrecords = fileParser.parseFile(filePath);
		log.info("Total records in file " + mainrecords.size());
		List<Record> endbanlaceMismatchRecords = FileUtil.getRecodsWhosEndBalanceMismatch(mainrecords);
		log.info("Total mismatch endbalance records in file " + endbanlaceMismatchRecords.size());
		List<Record> duplicateTransactionRefRecords = FileUtil.getDuplicatesTransactionReferanceNumbers(mainrecords);
		log.info("Total duplicate transaction ref records in file " + duplicateTransactionRefRecords.size());

		mainrecords.clear();

		mainrecords.addAll(duplicateTransactionRefRecords);
		mainrecords.addAll(endbanlaceMismatchRecords);
		duplicateTransactionRefRecords.clear();
		endbanlaceMismatchRecords.clear();

		mainrecords = mainrecords.stream().sorted(Comparator.comparingInt(Record::getTransactionReferanceNumber))
				.collect(Collectors.toList());

		if (mainrecords.size() > 0)
			generateReportData(mainrecords);

		flushWriter();

	}

	/* (non-Javadoc)
	 * @see com.rabobank.tools.customerstatementprocessor.IFileProcessor#getFileParser(java.nio.file.Path)
	 */
	public IFileParser getFileParser(Path filePath) {
		return (FileUtil.checkCSVFile().test(filePath)) ? csvFileParser : xmlFileParser;

	}

	/* (non-Javadoc)
	 * @see com.rabobank.tools.customerstatementprocessor.IFileProcessor#generateReportData(java.util.List)
	 */
	@Override
	public void generateReportData(List<Record> records) throws ApplicationProcessorException {
		Consumer<Record> consumeRec = (Record rec) -> appendRecord(rec);
		records.stream().forEach(consumeRec);

	}

	/**
	 * Append record.
	 *
	 * @param rec the rec
	 */
	private void appendRecord(Record rec) {
		try {
			bw.append(rec.toString());
			bw.newLine();
		} catch (IOException e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.rabobank.tools.customerstatementprocessor.IFileProcessor#initReportWriter(java.lang.String)
	 */
	@Override
	public void initReportWriter(String outputfile) throws ApplicationProcessorException {
		try {

			File file = new File(outputfile);

			// if file doesnt exists, then create it
			if (file.exists()) {
				file.delete();
			}
			if (!file.exists()) {
				file.createNewFile();
			}
			// true = append file
			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);
			bw.write(config.getOutputfileheader());
			bw.newLine();
			log.info("Output file is available on location:" + file.getAbsolutePath());
		} catch (IOException e) {
			throw new ApplicationProcessorException(e.getLocalizedMessage(), e);
		}

	}

	/**
	 * Flush writer.
	 *
	 * @throws ApplicationProcessorException the application processor exception
	 */
	private void flushWriter() throws ApplicationProcessorException {
		try {
			bw.flush();
		} catch (IOException e) {
			throw new ApplicationProcessorException(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Close writer.
	 *
	 * @throws ApplicationProcessorException the application processor exception
	 */
	private void closeWriter() throws ApplicationProcessorException {
		try {
			if (bw != null)
				bw.close();
		} catch (IOException e) {
			throw new ApplicationProcessorException(e.getLocalizedMessage(), e);
		}
	}
}
