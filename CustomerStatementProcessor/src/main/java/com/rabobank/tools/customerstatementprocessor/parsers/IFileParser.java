package com.rabobank.tools.customerstatementprocessor.parsers;

import java.nio.file.Path;
import java.util.List;

import com.rabobank.tools.customerstatementprocessor.beans.Record;
import com.rabobank.tools.customerstatementprocessor.exception.ApplicationProcessorException;

/**
 * The Interface IFileParser.
 */
public interface IFileParser {
	
	/**
	 * Parses the file.
	 *
	 * @param inputFile the input file
	 * @return the list
	 * @throws ApplicationProcessorException the application processor exception
	 */
	List<Record> parseFile(Path inputFile) throws ApplicationProcessorException;
}
